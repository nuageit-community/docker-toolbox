# Semantic Versioning Changelog

## [1.3.5](https://gitlab.com/nuageit-community/docker-toolbox/compare/1.3.4...1.3.5) (2024-01-13)


### :bug: Fixes

* aws cli ([5ae5823](https://gitlab.com/nuageit-community/docker-toolbox/commit/5ae58231f536082af1f16275949ef3b424da1597))

## [1.3.4](https://gitlab.com/nuageit-community/docker-toolbox/compare/1.3.3...1.3.4) (2024-01-13)


### :bug: Fixes

* copy image ([d5656cb](https://gitlab.com/nuageit-community/docker-toolbox/commit/d5656cbc5542e15a224e6d181b7d0b7b2c0c4c5e))

## [1.3.3](https://gitlab.com/nuageit-community/docker-toolbox/compare/1.3.2...1.3.3) (2024-01-13)


### :bug: Fixes

* dockerfile style ([38cc049](https://gitlab.com/nuageit-community/docker-toolbox/commit/38cc0491cbff58614c9e13e77ed4a18f0c1d3de8))

## [1.3.2](https://gitlab.com/nuageit-community/docker-toolbox/compare/1.3.1...1.3.2) (2024-01-12)


### :bug: Fixes

* remove dive ([bd05c52](https://gitlab.com/nuageit-community/docker-toolbox/commit/bd05c52203f012960d94d7af3e12a09ee939d9ca))
* var version ([2b5b806](https://gitlab.com/nuageit-community/docker-toolbox/commit/2b5b806373851036c80f25e8acca945f2f954474))

## [1.3.1](https://gitlab.com/nuageit-community/docker-toolbox/compare/1.3.0...1.3.1) (2024-01-12)


### :bug: Fixes

* image dive version ([2f70768](https://gitlab.com/nuageit-community/docker-toolbox/commit/2f70768dae0dc6bf886c07b2b9ed990b8fb94d0a))

## [1.3.0](https://gitlab.com/nuageit-community/docker-toolbox/compare/1.2.0...1.3.0) (2024-01-12)


### :sparkles: News

* add quotes ([4cf38d2](https://gitlab.com/nuageit-community/docker-toolbox/commit/4cf38d2df101df8c352b5a1c8d50da8a8bff269d))


### :bug: Fixes

* image install tools ([03f974b](https://gitlab.com/nuageit-community/docker-toolbox/commit/03f974b08d54346d6c5b63d3b34235c05ef23cff))
* remove empty line ([c19375c](https://gitlab.com/nuageit-community/docker-toolbox/commit/c19375c04e9c707d7fc7775e80473b6cab60bd65))

## [1.2.0](https://gitlab.com/nuageit-community/docker-toolbox/compare/1.1.0...1.2.0) (2024-01-12)


### :sparkles: News

* add commands ([402d5bb](https://gitlab.com/nuageit-community/docker-toolbox/commit/402d5bb31a83157733a68429d67272d3b3aec864))
* add install trivy ([ce6a92b](https://gitlab.com/nuageit-community/docker-toolbox/commit/ce6a92b06a246fdb2f1cf20d57e479be5a451801))

## [1.1.0](https://gitlab.com/nuageit-community/docker-toolbox/compare/1.0.0...1.1.0) (2024-01-12)


### :zap: Refactoring

* update file name ([ddf5dcf](https://gitlab.com/nuageit-community/docker-toolbox/commit/ddf5dcf3c619a9b39565f93b6707e9ed435c75d2))


### :bug: Fixes

* hooks setup ([d7d64da](https://gitlab.com/nuageit-community/docker-toolbox/commit/d7d64daf11c05f264974ea052b9677ae2a6efdf8))
* identation yaml ([dec8ae8](https://gitlab.com/nuageit-community/docker-toolbox/commit/dec8ae88d0d9f0b285a49f76538a8ec4dd0ce7c6))
* organize project setup ([83b2a6a](https://gitlab.com/nuageit-community/docker-toolbox/commit/83b2a6abc99dde9752a8bf2f774f4c5a53045efa))
* pre-commit and makefile ([046118a](https://gitlab.com/nuageit-community/docker-toolbox/commit/046118ab483e77df75d88cb00e19c0ff3358236f))
* setup docs and taskfile ([35a8888](https://gitlab.com/nuageit-community/docker-toolbox/commit/35a888890d8deee1dd467ed242d72c54e93c7176))


### :repeat: CI

* change include ([d919dc4](https://gitlab.com/nuageit-community/docker-toolbox/commit/d919dc45438796205346c623c5dec56c4f6fe5d0))
* change ref name ([bfb14ac](https://gitlab.com/nuageit-community/docker-toolbox/commit/bfb14ac42f9a4bc8844e78f49a12c622647b3e85))


### :memo: Docs

* add button ([2ed28bb](https://gitlab.com/nuageit-community/docker-toolbox/commit/2ed28bb742bf3f7d87935cd4f169d43be3bf797d))
* change readme ([8282ec2](https://gitlab.com/nuageit-community/docker-toolbox/commit/8282ec292ab67c0d0922f46c4f7e8ec8e593a566))
* change script ([81c1f4d](https://gitlab.com/nuageit-community/docker-toolbox/commit/81c1f4d7258b7c87f84bc466cc687009416ce2e6))
* pretty readme [skip ci] ([53ecf6c](https://gitlab.com/nuageit-community/docker-toolbox/commit/53ecf6c479e8b2a811fd633c1546e25614509f66))


### :sparkles: News

* add install cosign ([c6fe297](https://gitlab.com/nuageit-community/docker-toolbox/commit/c6fe29727c11e5b25f3990be1cc753e1e0e37f53))

## 1.0.0 (2023-03-06)


### :repeat: CI

* add include job ([5286464](https://gitlab.com/nuageit-community/docker-toolbox/commit/5286464cd3de6643f464fd089090715f832c247a))

## 1.0.0 (2023-03-03)


### :repeat: CI

* adicionando novo template pipeline ([94ed50e](https://gitlab.com/nuageit-community/aws-docker/commit/94ed50ed94f674dbb40988483b81cd0fc05d4dee))
* correcao include ([91b393c](https://gitlab.com/nuageit-community/aws-docker/commit/91b393c2f2bdf68031b6fac40e882a0b6d2ce8e0))


### :bug: Fixes

* dockerfile plugin version ([31e9e5c](https://gitlab.com/nuageit-community/aws-docker/commit/31e9e5c21758f29f1021a4a918b29a3194b446c8))
* dockerfile semantic release plugins versions ([2665fd2](https://gitlab.com/nuageit-community/aws-docker/commit/2665fd271668c072d4fa3b0bdffd25646371bbf5))
* setup configurations ([9c98f94](https://gitlab.com/nuageit-community/aws-docker/commit/9c98f94eaf976b9cb6c1ed9130502976b9cfa90a))


### :memo: Docs

* add content ([7378d48](https://gitlab.com/nuageit-community/aws-docker/commit/7378d48957059620f337f814d3789cf71dc3eae7))
* empty readme ([f468539](https://gitlab.com/nuageit-community/aws-docker/commit/f46853957f4fe9e0037ca1ffbbac046ade60b303))
* link git clone ([3d45285](https://gitlab.com/nuageit-community/aws-docker/commit/3d4528585003650943a54420b43c52da98d9981e))
* more information ([fdd774d](https://gitlab.com/nuageit-community/aws-docker/commit/fdd774d2c9213034780c5a3ec0f965b6dcf5a9bc))
* pretty content ([af513d6](https://gitlab.com/nuageit-community/aws-docker/commit/af513d67be56e966434ba257d5d67aa51d6f609d))
* remove topic ([f4d134a](https://gitlab.com/nuageit-community/aws-docker/commit/f4d134a67c25046b11f5ac78405e4a2938731948))
* resize image ([efd7462](https://gitlab.com/nuageit-community/aws-docker/commit/efd746231d42fe9c6a13224b8907d0a356e07907))


### :sparkles: News

* add config files and dockerfile ([76cbd79](https://gitlab.com/nuageit-community/aws-docker/commit/76cbd79cb469f5304d433acc8cc8b533300e28e5))
* add gitignore, editorconfig and pipeline ([c07907d](https://gitlab.com/nuageit-community/aws-docker/commit/c07907d66bb44105b0b72b9a517a7c362f092400))
* add google semantic release replace ([69b08e2](https://gitlab.com/nuageit-community/aws-docker/commit/69b08e2900108783a54813a8d2c8ad1ac2194c55))
* add helm-docs ([3c2451f](https://gitlab.com/nuageit-community/aws-docker/commit/3c2451fe9520dd282d833dbee0e98c34c0c681b3))
* add plugin google replace ([1a7db15](https://gitlab.com/nuageit-community/aws-docker/commit/1a7db1538a9b04a911347e916cc1a3b041133f7d))
* add semantic release exec ([1e94241](https://gitlab.com/nuageit-community/aws-docker/commit/1e94241326120d26179184457fd712a7bd55daa1))

## [1.3.0](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.2.1...1.3.0) (2023-01-18)


### :bug: Fixes

* setup configurations ([9c98f94](https://gitlab.com/nuageit-community/smr-toolbox/commit/9c98f94eaf976b9cb6c1ed9130502976b9cfa90a))


### :memo: Docs

* link git clone ([3d45285](https://gitlab.com/nuageit-community/smr-toolbox/commit/3d4528585003650943a54420b43c52da98d9981e))
* remove topic ([f4d134a](https://gitlab.com/nuageit-community/smr-toolbox/commit/f4d134a67c25046b11f5ac78405e4a2938731948))


### :sparkles: News

* add google semantic release replace ([69b08e2](https://gitlab.com/nuageit-community/smr-toolbox/commit/69b08e2900108783a54813a8d2c8ad1ac2194c55))

## [1.2.1](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.2.0...1.2.1) (2023-01-17)


### :memo: Docs

* more information ([fdd774d](https://gitlab.com/nuageit-community/smr-toolbox/commit/fdd774d2c9213034780c5a3ec0f965b6dcf5a9bc))
* resize image ([efd7462](https://gitlab.com/nuageit-community/smr-toolbox/commit/efd746231d42fe9c6a13224b8907d0a356e07907))


### :repeat: CI

* adicionando novo template pipeline ([94ed50e](https://gitlab.com/nuageit-community/smr-toolbox/commit/94ed50ed94f674dbb40988483b81cd0fc05d4dee))
* correcao include ([91b393c](https://gitlab.com/nuageit-community/smr-toolbox/commit/91b393c2f2bdf68031b6fac40e882a0b6d2ce8e0))

## [1.2.0](https://gitlab.com/nuageit/devops/smr-toolbox/compare/1.1.1...1.2.0) (2022-11-27)


### :sparkles: News

* add semantic release exec ([1e94241](https://gitlab.com/nuageit/devops/smr-toolbox/commit/1e94241326120d26179184457fd712a7bd55daa1))

## [1.1.1](https://gitlab.com/nuageit/devops/smr-toolbox/compare/1.1.0...1.1.1) (2022-11-27)


### :bug: Fixes

* dockerfile plugin version ([31e9e5c](https://gitlab.com/nuageit/devops/smr-toolbox/commit/31e9e5c21758f29f1021a4a918b29a3194b446c8))

## [1.1.0](https://gitlab.com/nuageit/devops/smr-toolbox/compare/1.0.0...1.1.0) (2022-11-27)


### :sparkles: News

* add plugin google replace ([1a7db15](https://gitlab.com/nuageit/devops/smr-toolbox/commit/1a7db1538a9b04a911347e916cc1a3b041133f7d))

## 1.0.0 (2022-11-27)


### :bug: Fixes

* dockerfile semantic release plugins versions ([2665fd2](https://gitlab.com/nuageit/devops/smr-toolbox/commit/2665fd271668c072d4fa3b0bdffd25646371bbf5))


### :sparkles: News

* add config files and dockerfile ([76cbd79](https://gitlab.com/nuageit/devops/smr-toolbox/commit/76cbd79cb469f5304d433acc8cc8b533300e28e5))
* add gitignore, editorconfig and pipeline ([c07907d](https://gitlab.com/nuageit/devops/smr-toolbox/commit/c07907d66bb44105b0b72b9a517a7c362f092400))
* add helm-docs ([3c2451f](https://gitlab.com/nuageit/devops/smr-toolbox/commit/3c2451fe9520dd282d833dbee0e98c34c0c681b3))


### :memo: Docs

* add content ([7378d48](https://gitlab.com/nuageit/devops/smr-toolbox/commit/7378d48957059620f337f814d3789cf71dc3eae7))
* empty readme ([f468539](https://gitlab.com/nuageit/devops/smr-toolbox/commit/f46853957f4fe9e0037ca1ffbbac046ade60b303))
* pretty content ([af513d6](https://gitlab.com/nuageit/devops/smr-toolbox/commit/af513d67be56e966434ba257d5d67aa51d6f609d))
