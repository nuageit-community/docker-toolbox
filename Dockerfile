# Base image for cosign binary
FROM gcr.io/projectsigstore/cosign:v2.2.2 as cosign-bin

# Final base image
FROM amazon/aws-cli:2.15.9
# Set environment variable
ENV DOCKER_HOST="tcp://docker:2375"
# Install Docker-cli
RUN amazon-linux-extras install docker
# Copy cosign from cosign-bin stage
COPY --from=cosign-bin ["/ko-app/cosign", "/usr/local/bin/cosign"]
# Copy trivy from its source
COPY --from=ghcr.io/aquasecurity/trivy:0.48.3 ["/usr/local/bin/trivy", "/usr/local/bin/trivy"]
# Set default command
ENTRYPOINT [ "/bin/bash" ]
